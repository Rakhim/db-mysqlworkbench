use mydb;
DROP FUNCTION random_word;
DELIMITER $$
CREATE FUNCTION random_word()
  RETURNS TEXT
  LANGUAGE SQL
BEGIN
  RETURN md5(floor(1 + rand()));
END;
$$
DELIMITER ;

